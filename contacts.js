"use strict";

// A LIST OF ALL SAVED CONTACTS
let savedContactsElements = JSON.parse(localStorage.getItem("storedContacts"));
const savedContactsList = localStorage !== null ? savedContactsElements : [];

const getStoredContacts = () => {
  for (let elem of savedContactsList.entries()) {
    const contactInformationContainer =
      document.getElementById("contactsAdded");

    //CREATING THE INFOTMATION CARD
    const informationCard = document.createElement("div");
    informationCard.setAttribute("class", "informationCard");
    contactInformationContainer.appendChild(informationCard);

    //CARD NAME
    const personTitle = document.createElement("h4");
    personTitle.setAttribute("class", "personTitle");
    personTitle.innerHTML = `Full Name: ${elem[1].thePersonName}`;
    informationCard.appendChild(personTitle);

    //CARD PHONE NUMBER
    const personPhoneNum = document.createElement("h4");
    personPhoneNum.innerHTML = `Phone number: ${elem[1].thePersonPhoneNumber}`;
    personPhoneNum.setAttribute("class", "phoneNumber");
    informationCard.appendChild(personPhoneNum);

    //CARD EMAIL
    const personEmail = document.createElement("h4");
    personEmail.innerHTML = `E-mail: ${elem[1].thePersonEmail}`;
    personEmail.setAttribute("class", "personEmail");
    informationCard.appendChild(personEmail);

    const deleteButton = document.createElement("button");
    deleteButton.innerHTML = "Delete Contact";
    deleteButton.setAttribute("class", "deleteBtn");
    deleteButton.onclick = function () {
      let index = savedContactsList.indexOf(elem[0][1]);
      savedContactsElements.splice(index, 1);
      localStorage.setItem(
        "storedContacts",
        JSON.stringify(savedContactsElements)
      );
      contactInformationContainer.removeChild(informationCard);
      console.log(savedContactsList);
    };
    informationCard.appendChild(deleteButton);
  }
};

getStoredContacts();
// ADD NEW CONTACT FUNCTION
function addNewContact() {
  //GETTING THE INPUT VALUES
  let newPersonName = document.getElementById("newPersonName").value;
  let newPersonPhoneNumber = document.getElementById(
    "newPersonPhoneNumber"
  ).value;
  let newPersonEmail = document.getElementById("newPersonEmail").value;

  // CREATING THE OBJECT TO BE SAVED IN LOCAL STORAGE, AND EVERY OBJECT INDEX IN THE ARRAY
  const savedContact = {
    thePersonName: newPersonName,
    thePersonPhoneNumber: newPersonPhoneNumber,
    thePersonEmail: newPersonEmail,
  };

  if (
    newPersonName === "" &&
    newPersonPhoneNumber === "" &&
    newPersonEmail === ""
  ) {
    alert("You should fill at least one field");
  } else {
    const contactInformationContainer =
      document.getElementById("contactsAdded");
    //CREATING THE INFOTMATION CARD
    const informationCard = document.createElement("div");
    informationCard.setAttribute("class", "informationCard");
    contactInformationContainer.appendChild(informationCard);
    //CREATING THE CARD ELEMENTS

    //CARD NAME
    const personTitle = document.createElement("h4");
    personTitle.setAttribute("class", "personTitle");
    personTitle.innerHTML = `Full Name: ${newPersonName}`;
    informationCard.appendChild(personTitle);

    //CARD PHONE NUMBER
    const personPhoneNum = document.createElement("h4");
    personPhoneNum.innerHTML = `Phone number: ${newPersonPhoneNumber}`;
    personPhoneNum.setAttribute("class", "phoneNumber");
    informationCard.appendChild(personPhoneNum);

    //CARD EMAIL
    const personEmail = document.createElement("h4");
    personEmail.innerHTML = `E-mail: ${newPersonEmail}`;
    personEmail.setAttribute("class", "personEmail");
    informationCard.appendChild(personEmail);

    //CARD DELETE BUTTON
    const deleteButton = document.createElement("button");
    deleteButton.innerHTML = "Delete Contact";
    deleteButton.setAttribute("class", "deleteBtn");
    deleteButton.onclick = function () {
      let index = savedContactsList.indexOf(savedContact);
      savedContactsList.splice(index, 1);
      localStorage.setItem("storedContacts", JSON.stringify(savedContactsList));
      contactInformationContainer.removeChild(informationCard);
    };
    informationCard.appendChild(deleteButton);

    // ADDING THE NEW ELEMETS, AND SAVE THEM
    savedContactsList.push(savedContact);
    localStorage.setItem("storedContacts", JSON.stringify(savedContactsList));
    console.log(savedContactsList);
    //CLEAR FIELDS
    document.getElementById("newPersonName").value = "";
    document.getElementById("newPersonPhoneNumber").value = "";
    document.getElementById("newPersonEmail").value = "";
  }
}
