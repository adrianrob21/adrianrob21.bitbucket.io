"use strict";

/* function addNewContact() {
  //GETTING THE INPUT VALUES
  let newPersonName = document.getElementById("newPersonName").value;
  let newPersonPhoneNumber = document.getElementById(
    "newPersonPhoneNumber"
  ).value;
  let newPersonEmail = document.getElementById("newPersonEmail").value;
  if (
    newPersonName === "" &&
    newPersonPhoneNumber === "" &&
    newPersonEmail === ""
  ) {
    alert("You shoul fill at least one field");
  } else {
    const contactInformationContainer =
      document.getElementById("contactsAdded");
    //CREATING THE INFOTMATION CARD
    const informationCard = document.createElement("div");
    informationCard.setAttribute("class", "informationCard");
    contactInformationContainer.appendChild(informationCard);
    //CREATING THE CARD ELEMENTS

    //CARD NAME
    const personTitle = document.createElement("h4");
    personTitle.innerHTML = `Full Name: ${newPersonName}`;
    personTitle.setAttribute("class", "personTitle");
    personTitle.contentEditable = true;
    informationCard.appendChild(personTitle);

    //CARD PHONE NUMBER
    const personPhoneNum = document.createElement("h4");
    personPhoneNum.innerHTML = `Phone number: ${newPersonPhoneNumber}`;
    personPhoneNum.setAttribute("class", "phoneNumber");
    personPhoneNum.contentEditable = true;
    informationCard.appendChild(personPhoneNum);

    //CARD EMAIL
    const personEmail = document.createElement("h4");
    personEmail.innerHTML = `E-mail: ${newPersonEmail}`;
    personEmail.setAttribute("class", "personEmail");
    personEmail.contentEditable = true;
    informationCard.appendChild(personEmail);

    //CARD DELETE BUTTON
    const deleteButton = document.createElement("button");
    deleteButton.innerHTML = "Delete Contact";
    deleteButton.setAttribute("class", "deleteBtn");
    deleteButton.onclick = function () {
      contactInformationContainer.removeChild(informationCard);
    };
    informationCard.appendChild(deleteButton);

    //CLEAR FIELDS
    document.getElementById("newPersonName").value = "";
    document.getElementById("newPersonPhoneNumber").value = "";
    document.getElementById("newPersonEmail").value = "";
  }
} */

/* function addNewTask() {
  //GETTING THE INPUT VALUES
  const taskTitleValue = document.getElementById("taskTitle").value;
  const taskDescriptionValue = document.getElementById("taskDescription").value;

  // DEFINING THE CARD CONTAINER
  const taskInformationContainer = document.getElementById("tasksAdded");
  if (taskTitleValue === "" && taskDescriptionValue === "") {
    alert("You should fill at least one field");
  } else {
    //CREATING THE CARD
    const taskCard = document.createElement("div");
    taskCard.setAttribute("class", "taskCard");
    taskInformationContainer.appendChild(taskCard);

    //CREATING THE TASK TITLE
    const taskTitle = document.createElement("h4");
    taskTitle.innerHTML = taskTitleValue;
    taskTitle.setAttribute("class", "taskTitle");
    taskCard.appendChild(taskTitle);

    //CREATING THE TASK DESCRIPTION
    const taskDescription = document.createElement("h4");
    taskDescription.innerHTML = taskDescriptionValue;
    taskDescription.setAttribute("class", "taskDescription");
    taskCard.appendChild(taskDescription);

    //CREATING THE DELETE BUTTON
    const taskDeleteBtn = document.createElement("button");
    taskDeleteBtn.innerHTML = "Delete Task";
    taskDeleteBtn.setAttribute("class", "deleteBtn");
    taskDeleteBtn.onclick = function () {
      taskInformationContainer.removeChild(taskCard);
    };
    taskCard.appendChild(taskDeleteBtn);
    //CLEAR FIELDS
    document.getElementById("taskTitle").value = "";
    document.getElementById("taskDescription").value = "";
  }
} */

/*function addNewApointment() {
  //GETTING INPUT VALUES
  const apointTitleValue = document.getElementById("apointTitle").value;
  const apointAdressValue = document.getElementById("apointAdress").value;
  const apointDateValue = document.getElementById("apointDate").value;
  const apointHoursValue = document.getElementById("apointHours").value;

  const apointMainContainer = document.getElementById("apointMainContainer");
  if (
    apointTitleValue === "" &&
    apointAdressValue === "" &&
    apointDateValue === "" &&
    apointHoursValue === ""
  ) {
    alert("You should fill at least one field");
  } else {
    //CREATING THE APOINTMENTS CARD
    const apointCard = document.createElement("div");
    apointCard.setAttribute("class", "apointContainer");
    apointMainContainer.appendChild(apointCard);

    //CREATING THE APOINTMENT TITLE
    const apointTitle = document.createElement("h3");
    apointTitle.setAttribute("class", "apointTitle");
    apointTitle.contentEditable = true;
    apointTitle.innerHTML = apointTitleValue;
    apointCard.appendChild(apointTitle);

    // CREATING THE APOINTMENT ADRESS
    const apointAdress = document.createElement("h4");
    apointAdress.setAttribute("class", "apointTitle");
    apointAdress.contentEditable = true;
    apointAdress.innerHTML = `Adress: ${apointAdressValue}`;
    apointCard.appendChild(apointAdress);

    // CREATING THE APOINTMENT DATE
    const apointDate = document.createElement("h4");
    apointDate.setAttribute("class", "apointDate");
    apointDate.contentEditable = true;
    apointDate.innerHTML = `Date: ${apointDateValue}`;
    apointCard.appendChild(apointDate);

    // CREATING THE APOINTMENT HOURS
    const apointHours = document.createElement("h4");
    apointHours.setAttribute("class", "apointHours");
    apointHours.contentEditable = true;
    apointHours.innerHTML = `Time: ${apointHoursValue}`;
    apointCard.appendChild(apointHours);

    // CREATING THE DELETE BUTTON
    const deleteApoint = document.createElement("button");
    deleteApoint.setAttribute("class", "deleteBtn");
    deleteApoint.innerHTML = "Delete Apointment";
    deleteApoint.onclick = function () {
      apointMainContainer.removeChild(apointCard);
    };
    apointCard.appendChild(deleteApoint);

    // CLEARING THE INPUT FIELDS
    document.getElementById("apointTitle").value = "";
    document.getElementById("apointAdress").value = "";
    document.getElementById("apointDate").value = "";
    document.getElementById("apointHours").value = "";
  }
} */
