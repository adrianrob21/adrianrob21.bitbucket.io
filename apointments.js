"use strict";
let apointmentsElements = JSON.parse(localStorage.getItem("savedApoints"));
let apointmentsList =
  localStorage.getItem("savedApoints") !== null ? apointmentsElements : [];

const getSavedApoint = () => {
  for (let elem of apointmentsList.entries()) {
    const apointMainContainer = document.getElementById("apointMainContainer");

    //CREATING THE APOINTMENTS CARD
    const apointCard = document.createElement("div");
    apointCard.setAttribute("class", "apointContainer");
    apointMainContainer.appendChild(apointCard);

    //CREATING THE APOINTMENT TITLE
    const apointTitle = document.createElement("h3");
    apointTitle.setAttribute("class", "apointTitle");
    apointTitle.innerHTML = elem[1].title;
    apointCard.appendChild(apointTitle);

    // CREATING THE APOINTMENT ADRESS
    const apointAdress = document.createElement("h4");
    apointAdress.setAttribute("class", "apointTitle");
    apointAdress.innerHTML = `Adress: ${elem[1].adress}`;
    apointCard.appendChild(apointAdress);

    // CREATING THE APOINTMENT DATE
    const apointDate = document.createElement("h4");
    apointDate.setAttribute("class", "apointDate");
    apointDate.innerHTML = `Date: ${elem[1].date}`;
    apointCard.appendChild(apointDate);

    // CREATING THE APOINTMENT HOURS
    const apointHours = document.createElement("h4");
    apointHours.setAttribute("class", "apointHours");
    apointHours.innerHTML = `Time: ${elem[1].hours}`;
    apointCard.appendChild(apointHours);

    // CREATING THE DELETE BUTTON
    const deleteApoint = document.createElement("button");
    deleteApoint.setAttribute("class", "deleteBtn");
    deleteApoint.innerHTML = "Delete Apointment";
    deleteApoint.onclick = function () {
      let index = apointmentsList.indexOf(elem[0][1]);
      apointmentsList.splice(index, 1);
      localStorage.setItem("savedApoints", JSON.stringify(apointmentsElements));
      apointMainContainer.removeChild(apointCard);
    };
    apointCard.appendChild(deleteApoint);
  }
};
getSavedApoint();

function addNewApoint() {
  //GETTING INPUT VALUES
  const apointTitleValue = document.getElementById("apointTitle").value;
  const apointAdressValue = document.getElementById("apointAdress").value;
  const apointDateValue = document.getElementById("apointDate").value;
  const apointHoursValue = document.getElementById("apointHours").value;

  // CREATING THE OBJECT TO BE SAVED ON LOCAL STORAGE
  const apointObj = {
    title: apointTitleValue,
    adress: apointAdressValue,
    date: apointDateValue,
    hours: apointHoursValue,
  };

  //////////////////////////////////////////////////////
  const apointMainContainer = document.getElementById("apointMainContainer");
  if (
    apointTitleValue === "" &&
    apointAdressValue === "" &&
    apointDateValue === "" &&
    apointHoursValue === ""
  ) {
    alert("You should fill at least one field");
  } else {
    //CREATING THE APOINTMENTS CARD
    const apointCard = document.createElement("div");
    apointCard.setAttribute("class", "apointContainer");
    apointMainContainer.appendChild(apointCard);

    //CREATING THE APOINTMENT TITLE
    const apointTitle = document.createElement("h3");
    apointTitle.setAttribute("class", "apointTitle");
    apointTitle.innerHTML = apointTitleValue;
    apointCard.appendChild(apointTitle);

    // CREATING THE APOINTMENT ADRESS
    const apointAdress = document.createElement("h4");
    apointAdress.setAttribute("class", "apointTitle");
    apointAdress.contentEditable = true;
    apointAdress.innerHTML = `Adress: ${apointAdressValue}`;
    apointCard.appendChild(apointAdress);

    // CREATING THE APOINTMENT DATE
    const apointDate = document.createElement("h4");
    apointDate.setAttribute("class", "apointDate");
    apointDate.innerHTML = `Date: ${apointDateValue}`;
    apointCard.appendChild(apointDate);

    // CREATING THE APOINTMENT HOURS
    const apointHours = document.createElement("h4");
    apointHours.setAttribute("class", "apointHours");
    apointHours.innerHTML = `Time: ${apointHoursValue}`;
    apointCard.appendChild(apointHours);

    // CREATING THE DELETE BUTTON
    const deleteApoint = document.createElement("button");
    deleteApoint.setAttribute("class", "deleteBtn");
    deleteApoint.innerHTML = "Delete Apointment";
    deleteApoint.onclick = function () {
      let index = apointmentsList.indexOf(apointObj);
      apointmentsList.splice(index, 1);
      localStorage.setItem("savedApoints", JSON.stringify(apointmentsList));
      apointMainContainer.removeChild(apointCard);
    };
    apointCard.appendChild(deleteApoint);

    //**************************************************
    apointmentsList.push(apointObj);
    localStorage.setItem("savedApoints", JSON.stringify(apointmentsList));

    // CLEARING THE INPUT FIELDS
    document.getElementById("apointTitle").value = "";
    document.getElementById("apointAdress").value = "";
    document.getElementById("apointDate").value = "";
    document.getElementById("apointHours").value = "";
  }
}
