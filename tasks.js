"use strict";
// A LIST OF ALL SAVED TASKS
const savedTasksElements = JSON.parse(localStorage.getItem("storedTask"));
const savedTasks =
  localStorage.getItem("storedTask") !== null ? savedTasksElements : [];

const getStoredTask = () => {
  for (let elemT of savedTasks.entries()) {
    const taskInformationContainer = document.getElementById("tasksAdded");

    //CREATING THE CARD
    const taskCard = document.createElement("div");
    taskCard.setAttribute("class", "taskCard");
    taskInformationContainer.appendChild(taskCard);

    //CREATING THE TASK TITLE
    const taskTitle = document.createElement("h4");
    taskTitle.innerHTML = elemT[1].taskName;
    taskTitle.setAttribute("class", "taskTitle");
    taskCard.appendChild(taskTitle);

    //CREATING THE TASK DESCRIPTION
    const taskDescription = document.createElement("h4");
    taskDescription.innerHTML = elemT[1].taskDesc;
    taskDescription.setAttribute("class", "taskDescription");
    taskCard.appendChild(taskDescription);

    //CREATING THE DELETE BUTTON
    const taskDeleteBtn = document.createElement("button");
    taskDeleteBtn.innerHTML = "Delete Task";
    taskDeleteBtn.setAttribute("class", "deleteBtn");
    taskDeleteBtn.onclick = function () {
      let index = savedTasks.indexOf(elemT[1]);
      savedTasks.splice(elemT[index], 1);
      localStorage.setItem("storedTask", JSON.stringify(savedTasks));
      taskInformationContainer.removeChild(taskCard);
    };
    taskCard.appendChild(taskDeleteBtn);
  }
};
getStoredTask();

// THE FUNCTION THAT ADDS A NEW TASK
function addNewTask() {
  //GETTING THE INPUT VALUES
  const taskTitleValue = document.getElementById("taskTitle").value;
  const taskDescriptionValue = document.getElementById("taskDescription").value;

  //CREATING THE OBJECT TO BE STORED, AND GETTING HIS INDEX
  const savedTask = {
    taskName: taskTitleValue,
    taskDesc: taskDescriptionValue,
  };

  // DEFINING THE CARD CONTAINER
  const taskInformationContainer = document.getElementById("tasksAdded");
  if (taskTitleValue === "" && taskDescriptionValue === "") {
    alert("You should fill at least one field");
  } else {
    //CREATING THE CARD
    const taskCard = document.createElement("div");
    taskCard.setAttribute("class", "taskCard");
    taskInformationContainer.appendChild(taskCard);

    //CREATING THE TASK TITLE
    const taskTitle = document.createElement("h4");
    taskTitle.innerHTML = taskTitleValue;
    taskTitle.setAttribute("class", "taskTitle");
    taskCard.appendChild(taskTitle);

    //CREATING THE TASK DESCRIPTION
    const taskDescription = document.createElement("h4");
    taskDescription.innerHTML = taskDescriptionValue;
    taskDescription.setAttribute("class", "taskDescription");
    taskCard.appendChild(taskDescription);

    //CREATING THE DELETE BUTTON
    const taskDeleteBtn = document.createElement("button");
    taskDeleteBtn.innerHTML = "Delete Task";
    taskDeleteBtn.setAttribute("class", "deleteBtn");
    taskDeleteBtn.onclick = function () {
      let indexOfTask = savedTasks.indexOf(savedTask);
      savedTasks.splice(indexOfTask, 1);
      localStorage.setItem("storedTask", JSON.stringify(savedTasks));
      taskInformationContainer.removeChild(taskCard);
    };
    taskCard.appendChild(taskDeleteBtn);

    //*********************************/
    savedTasks.push(savedTask);
    localStorage.setItem("storedTask", JSON.stringify(savedTasks));
    console.log(savedTasks);

    //CLEAR FIELDS
    document.getElementById("taskTitle").value = "";
    document.getElementById("taskDescription").value = "";
  }
}
